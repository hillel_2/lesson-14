from django.urls import path

from market import views

app_name = 'market'

urlpatterns = [
    path('', views.list, name='list'),
    path('view/<item_id>/', views.item, name='item'),
    path('add/', views.add, name='add'),
]