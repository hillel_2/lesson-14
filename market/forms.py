from django import forms

from market.models import Wallpaper


class WallpaperForm(forms.ModelForm):
    class Meta:
        model = Wallpaper
        fields = ('name', 'brand', 'tags', 'color', 'price', 'available', 'quantity', 'description', 'cover')
