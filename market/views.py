from django.shortcuts import render, redirect

from market.forms import WallpaperForm
from market.models import Wallpaper


def list(request):
    wallpapers = Wallpaper.objects.all()
    return render(request,
                  'market/wallpapers.html',
                  context={'wallpapers': wallpapers})


def item(request, item_id):
    wallpaper = Wallpaper.objects.get(slug=item_id)
    tags = wallpaper.tags.all()
    return render(request,
                  'market/wallpaper.html',
                  context={'wallpaper': wallpaper, 'tags': tags})


def add(request):
    if request.method == 'POST':
        form = WallpaperForm(request.POST, request.FILES)
        if form.is_valid():
            new_wallpaper = form.save(commit=False)
            new_wallpaper.creator = request.user
            new_wallpaper.save()
            form.save_m2m()
            return redirect('market:item', item_id=new_wallpaper.slug)
        return render(request, 'market/new_wallpaper.html', context={'form': form})
    form = WallpaperForm()
    return render(request, 'market/new_wallpaper.html', context={'form': form})
