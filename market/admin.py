from django.contrib import admin

from market.models import Wallpaper, Brand, Tags

admin.site.register(Brand)
admin.site.register(Tags)


@admin.register(Wallpaper)
class WallpeperAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name', 'color')}
